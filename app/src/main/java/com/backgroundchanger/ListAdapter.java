package com.backgroundchanger;

import android.app.WallpaperManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter {
    List<ImageListItem> items = new ArrayList<ImageListItem>() {{
        add(new ImageListItem(R.drawable.img1, "Картинка 1"));
        add(new ImageListItem(R.drawable.img2, "Картинка 2"));
        add(new ImageListItem(R.drawable.img3, "Картинка 3"));
    }};

    public ListAdapter() {
    }

    public ListAdapter(List<ImageListItem> items) {
        this.items = items;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ImageListItem item = items.get(position);
        ((ItemViewHolder) holder).getPreviewImage().setImageResource(item.getImgResId());
        ((ItemViewHolder) holder).getImgName().setText(item.getName());

        ((ItemViewHolder) holder).v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(v.getContext());
                try {
                    myWallpaperManager.setResource(item.getImgResId());
                    Toast.makeText(v.getContext(), "Изображение изменено", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private static class ItemViewHolder extends RecyclerView.ViewHolder {
        ImageView previewImage;
        TextView imgName;
        View v;

        public ItemViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            previewImage = (ImageView) itemView.findViewById(R.id.previewImage);
            imgName = (TextView) itemView.findViewById(R.id.imgName);
        }

        public ImageView getPreviewImage() {
            return previewImage;
        }

        public TextView getImgName() {
            return imgName;
        }
    }
}
