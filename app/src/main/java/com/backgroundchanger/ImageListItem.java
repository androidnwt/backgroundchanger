package com.backgroundchanger;

/**
 * Created by nonbl on 15.01.2017.
 */

public class ImageListItem {
    private int imgResId;
    private String name;

    public ImageListItem(int imgResId, String name) {
        this.imgResId = imgResId;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }
}
